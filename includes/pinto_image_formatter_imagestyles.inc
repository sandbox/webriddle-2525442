<?php

/**
 * @file
 * Provides default Image styles presets.
 */

/**
 * Implements hook_image_default_styles().
 */
function pinto_image_formatter_image_default_styles() {
  $styles = array();

  $styles['pinto100x'] = array(
    'label' => 'Pinto (100x)',
    'effects' => array(
      array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 100,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 0,
      ),
    ),
  );
  $styles['pinto220x'] = array(
    'label' => 'Pinto (220x)',
    'effects' => array(
      array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 220,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 0,
      ),
    ),
  );
  $styles['pinto480x'] = array(
    'label' => 'Pinto (480x)',
    'effects' => array(
      array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 480,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 0,
      ),
    ),
  );
  return $styles;
}
